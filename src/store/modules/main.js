import { calculateForce } from "@/common/helpers/calculateForce";
import { getRandomInt } from "@/common/helpers/randomInteger";
import { boxShapes } from "@/common/helpers/boxShapes";
import { SPEED } from "@/common/constants/game";

const state = () => ({
  isGamePaused: false,
  isArrowRightDown: false,
  isArrowLeftDown: false,
  balanceSideLimits: 0,
  balanceRotationDeg: 0,
  totalLeftWeight: 0,
  totalRightWeight: 0,
  force: { leftForce: 0, rightForce: 0 },
  rightWeights: [
    {
      type: "circle",
      weight: 3,
      isFalling: true,
      horizontalPositionProbability: 0.7,
      positionY: -100,
    },
  ],
  weights: [
    {
      type: "triangle",
      weight: 5,
      isFalling: true,
      horizontalPositionProbability: 0.3,
      positionY: -250,
    },
  ],
});

// getters
const getters = {
  getBalanceLimits: (state) => {
    return state.balanceSideLimits;
  },
  getWeights: (state) => {
    return state.weights;
  },
  getRandomWeights: (state) => {
    return state.rightWeights;
  },

  getMomentum(state) {
    return calculateForce(state.rightWeights, state.weights);
  },
  getTotalWeight(state) {
    return {
      totalRightWeight: state.totalRightWeight,
      totalLeftWeight: state.totalLeftWeight,
    };
  },
  getGameState(state) {
    return state.isGamePaused;
  },
  getIsArrowRightDown(state) {
    return state.isArrowRightDown;
  },
  getIsArrowLeftDown(state) {
    return state.isArrowLeftDown;
  },
};

const actions = {
  setBalanceSideLimit({ commit }, limit) {
    commit("setBalanceLimit", limit);
  },
  setBalanceRotationDeg({ commit }, degree) {
    commit("setBalanceRotationDeg", degree);
  },
  pushNewWeight({ commit }, weight) {
    commit("pushNewWeight", weight);
  },
  updateWeightFalling({ commit }, { index, isFalling }) {
    commit("updateWeightFalling", { index, isFalling });
    commit("pushNewWeight");
  },
  updateRightWeightFalling({ commit }, { index, isFalling }) {
    commit("updateRightWeightFalling", { index, isFalling });
    commit("pushNewWeightToRight");
  },
  updateWeightPosition({ commit }, index) {
    commit("updateWeightPosition", index);
  },
  updateRightWeightPosition({ commit }, index) {
    commit("updateRightWeightPosition", index);
  },
  setFalledWeightPosition({ commit }, { index, position }) {
    commit("setFalledWeightPosition", { index, position });
  },
  setFalledRightWeightPosition({ commit }, { index, position }) {
    commit("setFalledRightWeightPosition", { index, position });
  },
  setTotalRightWeight({ commit }, weight) {
    commit("setTotalRightWeight", weight);
  },
  setIsArrowLeftDown({ commit }, status) {
    commit("setIsArrowLeftDown", status);
  },
  setIsArrowRightDown({ commit }, status) {
    commit("setIsArrowRightDown", status);
  },
  moveLeft({ commit }, index) {
    commit("moveLeft", index);
  },
  moveRight({ commit }, index) {
    commit("moveRight", index);
  },
  setTotalLeftWeight({ commit }, weight) {
    commit("setTotalLeftWeight", weight);
  },
  setGameState({ commit }, state) {
    commit("setGameState", state);
  },
};

// mutations
const mutations = {
  setBalanceLimit(state, { balanceSideLimit }) {
    state.balanceSideLimits = balanceSideLimit;
  },
  setBalanceRotationDeg(state, { balanceRotationDeg }) {
    state.balanceRotationDeg = balanceRotationDeg;
  },
  pushNewWeight(state) {
    // add new box to the left side when another box ended falling
    const weight = getRandomInt(1, 10);
    const positionProbability = getRandomInt(1, 7) / 10;
    const shapeProbability = getRandomInt(0, 2);
    state.weights.push({
      type: boxShapes[shapeProbability],
      weight: weight,
      isFalling: true,
      horizontalPositionProbability: positionProbability,
      positionY: -100,
    });
  },
  pushNewWeightToRight(state) {
    // add new box to the right side when another box ended falling
    const weight = getRandomInt(1, 10);
    const positionProbability = getRandomInt(1, 7) / 10;
    const shapeProbability = getRandomInt(0, 2);
    state.rightWeights.push({
      type: boxShapes[shapeProbability],
      weight: weight,
      isFalling: true,
      horizontalPositionProbability: positionProbability,
      positionY: -100,
    });
  },
  updateWeightPosition(state, index) {
    state.weights[index].positionY += SPEED;
  },
  updateRightWeightPosition(state, index) {
    state.rightWeights[index].positionY += SPEED;
  },
  updateWeightFalling(state, { index, isFalling }) {
    state.weights[index].isFalling = isFalling;
  },
  updateRightWeightFalling(state, { index, isFalling }) {
    state.rightWeights[index].isFalling = isFalling;
  },
  setFalledWeightPosition(state, { index, position }) {
    state.weights[index].positionY = position;
  },
  setFalledRightWeightPosition(state, { index, position }) {
    state.rightWeights[index].positionY = position;
  },
  setIsArrowLeftDown(state, status) {
    state.isArrowLeftDown = status;
  },
  setIsArrowRightDown(state, status) {
    state.isArrowRightDown = status;
  },
  moveLeft(state, index) {
    if (state.weights[index].horizontalPositionProbability > 0)
      state.weights[index].horizontalPositionProbability -= 0.003;
  },
  moveRight(state, index) {
    if (state.weights[index].horizontalPositionProbability < 0.8)
      state.weights[index].horizontalPositionProbability += 0.003;
  },
  setTotalRightWeight(state, weight) {
    state.totalRightWeight = weight;
  },
  setTotalLeftWeight(state, weight) {
    state.totalLeftWeight = weight;
  },
  setGameState(state, gameState) {
    state.isGamePaused = gameState;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
