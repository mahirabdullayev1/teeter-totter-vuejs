const state = () => ({
  balanceTopPosition: 0,
  rotationAngle: 0,
  rotationDelta: 0.05,
  rightSideForce: 0,
  leftSideForce: 0,
});

// getters
const getters = {
  getRotationDelta: (state) => {
    return state.rotationDelta;
  },
  getRotationAngle: (state) => {
    return state.rotationAngle;
  },
  getBalanceTopPosition: (state) => {
    return state.balanceTopPosition;
  },
};

// actions
const actions = {
  setBalanceRotationDeg({ commit }) {
    commit("setBalanceRotationDeg");
  },
  setBalanceLineTopPosition({ commit }, positionY) {
    commit("setBalanceLineTopPosition", positionY);
  },
  setRotationDelta({ commit }, rotationDelta) {
    commit("setRotationDelta", rotationDelta);
  },
};

// mutations
const mutations = {
  setBalanceRotationDeg(state) {
    state.rotationAngle = state.rotationAngle + state.rotationDelta;
  },
  setBalanceLineTopPosition(state, positionY) {
    state.balanceTopPosition = positionY;
  },
  setRotationDelta(state, rotationDelta) {
    state.rotationDelta = rotationDelta;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
