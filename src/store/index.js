import { createStore, createLogger } from "vuex";
import main from "./modules/main";
import balance from "@/store/modules/balance";

const debug = false;

export default createStore({
  modules: {
    main,
    balance,
  },
  strict: debug,
  plugins: debug ? [createLogger()] : [],
});
