import { onMounted, onUnmounted } from "vue";

export const listenKeyboradEvents = (store) => {
  const keyDownListener = (e) => {
    if (e.keyCode === 37) {
      store.dispatch("main/setIsArrowLeftDown", true);
    } else if (e.keyCode === 39) {
      store.dispatch("main/setIsArrowRightDown", true);
    }
  };
  const keyUpListener = (e) => {
    if (e.keyCode === 37) {
      store.dispatch("main/setIsArrowLeftDown", false);
    } else if (e.keyCode === 39) {
      store.dispatch("main/setIsArrowRightDown", false);
    }
  };
  onMounted(() => window.addEventListener("keydown", keyDownListener));
  onMounted(() => window.addEventListener("keyup", keyUpListener));
  onUnmounted(() => window.removeEventListener("keydown", keyDownListener));
  onUnmounted(() => window.removeEventListener("keyUp", keyUpListener));
};
